pytest-ldap
=============

pytest fixtures for ldap

Install with `pip install pytest-ldap`.

Then you can use the fixtures `slapd_server` and `slapd_connection` in your tests.
