import ldap
import slapdtest
import ldap.ldapobject


def _test_conn(server, conn):
    ou_dn = "ou=Container," + server.suffix
    conn.add_ext_s(
        ou_dn, [("objectClass", [b"organizationalUnit"]), ("ou", [b"Container"]),]
    )

    dn = "cn=Added,ou=Container," + server.suffix
    conn.add_ext_s(dn, [("objectClass", [b"organizationalRole"]), ("cn", [b"Added"]),])

    result = conn.search_s(server.suffix, ldap.SCOPE_SUBTREE, "(cn=Added)", ["*"])
    assert [
        (
            "cn=Added,ou=Container," + server.suffix,
            {"cn": [b"Added"], "objectClass": [b"organizationalRole"]},
        ),
    ] == result

    conn.delete_s(dn)
    conn.delete_s(ou_dn)
    result = conn.search_s(server.suffix, ldap.SCOPE_SUBTREE, "(cn=Added)", ["*"])
    assert [] == result


def test_server(slapd_server):
    assert isinstance(slapd_server, slapdtest.SlapdObject)

    conn = ldap.initialize(slapd_server.ldap_uri)
    conn.simple_bind_s(slapd_server.root_dn, slapd_server.root_pw)
    assert isinstance(conn, ldap.ldapobject.LDAPObject)

    _test_conn(slapd_server, conn)

    conn.unbind_s()


def test_connection(slapd_server, slapd_connection):
    _test_conn(slapd_server, slapd_connection)
    assert isinstance(slapd_connection, ldap.ldapobject.LDAPObject)
